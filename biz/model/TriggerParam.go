package model

import (
	"encoding/json"
)

type TriggerParam struct {
	JobId int32 `json:"jobId"`

	ExecutorHandler       string `json:"executorHandler"`
	ExecutorParams        string `json:"executorParams"`
	ExecutorBlockStrategy string `json:"executorBlockStrategy"`
	ExecutorTimeout       int    `json:"executorTimeout"`  //秒

	LogId       int64 `json:"logId"`
	LogDateTime int64 `json:"logDateTime"`

	GlueType       string `json:"glueType"`
	GlueSource     string `json:"glueSource"`
	GlueUpdatetime int64  `json:"glueUpdatetime"`

	BroadcastIndex int32 `json:"broadcastIndex"`
	BroadcastTotal int32 `json:"broadcastTotal"`
}

func (t TriggerParam) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}
