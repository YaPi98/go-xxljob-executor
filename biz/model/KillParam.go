package model

import (
	"encoding/json"
)

type KillParam struct {
	JobId int32 `json:"jobId"`
}

func (t KillParam) String() string {
	data, err := json.Marshal(t)
	if err != nil {
		return ""
	}

	return string(data)
}
