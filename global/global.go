package global

import (
	"gitee.com/YaPi98/go-xxljob-executor/biz/client"
	"go.uber.org/zap"
)

var (
	Logger       *zap.Logger
	AdminBizList []*client.AdminBizClient
	XxlLogPath   string
)
