package server

type XxlRpcResponse struct {
	RequestId string      `hessian:"requestId" json:"requestId"`
	ErrorMsg  string      `hessian:"errorMsg" json:"errorMsg"`
	Result    interface{} `hessian:"result" json:"result"`
}

func (XxlRpcResponse) JavaClassName() string {
	return "com.xxl.rpc.remoting.net.params.XxlRpcResponse"
}

// 解决xxl-rpc 中返回 errorMsg 为空字符仍报错问题(xxl-rpc的坑)
type XxlRpcResponse2 struct {
	RequestId string      `hessian:"requestId" json:"requestId"`
	ErrorMsg  interface{} `hessian:"errorMsg" json:"errorMsg"`
	Result    interface{} `hessian:"result" json:"result"`
}

func NewXxlRpcResponse2(r *XxlRpcResponse) XxlRpcResponse2 {
	return XxlRpcResponse2{
		RequestId: r.RequestId,
		ErrorMsg:  nil,
		Result:    r.Result,
	}
}

func (XxlRpcResponse2) JavaClassName() string {
	return "com.xxl.rpc.remoting.net.params.XxlRpcResponse"
}
